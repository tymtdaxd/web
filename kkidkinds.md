### 问：金刚号有哪些类型？
答：
- 在[ 金刚网>菜单>商店 ](https://atozitpro.net/shop/)里，[ 金刚号 ](/kkid.md)被划分为：
  - 1、<font color="Red">L2型</font>[ 金刚号 ](/kkid.md)，即[ 万能金刚号 ](/multipurposekkid.md)，可配入多种设备，其外形特征是非c9开头
  - 2、<font color="Red">SSL型</font>[ 金刚号 ](/kkid.md)，即[ 普通金刚号 ](/singlepurposekkid.md)，仅可用于Windows+SSL客户端环境中，其外形特征是c9开头

#### 推荐阅读

- [金刚梯](/dlb.md)
- [金刚帮助](/list_helpkkvpn.md)
- [金刚1.0金刚号梯](//list_helpkkvpn1.0.md)
- [金刚号](/list_kkid.md)
