###### 金刚梯>金刚帮助>金刚1.0金刚号梯帮助>术语 金刚号类>
### 问：金刚号与流量是什么关系？

答：

- [ 金刚流量 ](/kkdatatraffic.md)以[ 包 ](/kkdatatrafficpackage.md)为单位捆绑在[ 金刚号 ](/kkid.md)上供[ 金刚用户 ](/kkuser.md)使用


#### 推荐阅读
- [金刚梯](/dlb.md)
- [金刚帮助](/list_helpkkvpn.md)
- [金刚1.0金刚号梯](/list_helpkkvpn1.0.md)
- [术语 金刚号类](/list_kkid.md)
